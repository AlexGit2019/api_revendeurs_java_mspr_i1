package org.kawa.api_revendeurs.domain;

public class InvalidUsernameException extends RuntimeException {
    public InvalidUsernameException() {
        super("Le nom d'utilisateur fourni ne doit pas être vide");
    }
}
