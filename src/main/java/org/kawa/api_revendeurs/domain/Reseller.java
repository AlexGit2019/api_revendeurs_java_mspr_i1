package org.kawa.api_revendeurs.domain;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

public class Reseller {
    // Source de la Regex : https://www.w3resource.com/javascript/form/email-validation.php
    private static final Pattern MAIL_REGEX = Pattern.compile("^\\w+([-\\.\\+]\\w+)*@\\w+([-\\.]\\w+)*\\.\\w+$");

    private final String username;
    private final String email;

    public Reseller(String username, String email) {
        this.username = Optional.ofNullable(username)
                .map(String::trim)
                .filter(name -> !name.isEmpty())
                .orElseThrow(() -> new InvalidUsernameException());
        this.email = Optional.ofNullable(email)
                .map(String::trim)
                .filter(em -> MAIL_REGEX.matcher(em).matches())
                .orElseThrow(() -> new InvalidEmailException());
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Reseller other) && this.username.equals(other.username) && this.email.equals(other.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email);
    }
}
