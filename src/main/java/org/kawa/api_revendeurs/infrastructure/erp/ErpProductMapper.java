package org.kawa.api_revendeurs.infrastructure.erp;

import org.example.domaine.DateTimeMapper;
import org.example.domaine.NumberMapper;
import org.example.infrastructure.ErpProductDTO;
import org.kawa.api_revendeurs.domain.Product;
import org.kawa.api_revendeurs.infrastructure.MapstructSettings;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(config = MapstructSettings.class, uses = {DateTimeMapper.class, NumberMapper.class})
public interface ErpProductMapper {

    @Mapping(target = "price", source = "details.price")
    @Mapping(target = "description", source = "details.description")
    @Mapping(target = "color", source = "details.color")
    Product fromErpProductDto(ErpProductDTO productDTO);

    List<Product> fromErpProductDtos(List<ErpProductDTO> productDTO);
}
