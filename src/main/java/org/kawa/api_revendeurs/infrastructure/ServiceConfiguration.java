package org.kawa.api_revendeurs.infrastructure;

import org.kawa.api_revendeurs.services.ProductService;
import org.kawa.api_revendeurs.services.ProductStorageInterface;
import org.kawa.api_revendeurs.services.ResellerService;
import org.kawa.api_revendeurs.services.ResellerStorageInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {
    @Bean
    ProductService productService(ProductStorageInterface productStorageInterface) {
        return new ProductService(productStorageInterface);
    }

    @Bean
    ResellerService resellerService(ResellerStorageInterface resellerStorageInterface) {
        return new ResellerService(resellerStorageInterface);
    }
}
