package org.kawa.api_revendeurs.infrastructure.api.mapping;

import org.kawa.api_revendeurs.domain.Product;
import org.kawa.api_revendeurs.infrastructure.MapstructSettings;
import org.kawa.api_revendeurs.infrastructure.api.serialization.ProductApiDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(config = MapstructSettings.class, uses = { PositiveNumberMapper.class, InstantMapper.class })
public interface ProductApiDtoMapper {

    Product toProduct(ProductApiDto productApiDto);

    List<Product> toProductList(List<ProductApiDto> productApiDtoList);

    List<ProductApiDto> toProductDtoList(List<Product> products);

    ProductApiDto toProductDto(Product product);
}
