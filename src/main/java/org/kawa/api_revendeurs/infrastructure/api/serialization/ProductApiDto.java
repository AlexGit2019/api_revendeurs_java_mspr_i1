package org.kawa.api_revendeurs.infrastructure.api.serialization;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public record ProductApiDto(String id, OffsetDateTime createdAt, String name, BigDecimal price, String description, String color, int stock) {
}
