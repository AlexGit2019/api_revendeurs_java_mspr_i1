package org.kawa.api_revendeurs.infrastructure.api.serialization;

public record UserDto(String username, String email) {
}
