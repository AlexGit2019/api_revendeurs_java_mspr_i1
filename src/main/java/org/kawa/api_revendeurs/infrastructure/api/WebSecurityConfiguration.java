package org.kawa.api_revendeurs.infrastructure.api;

import org.kawa.api_revendeurs.services.AuthenticationInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.SecurityFilterChain;

import java.util.Collection;
import java.util.List;

// https://docs.spring.io/spring-security/reference/features/authentication/password-storage.html
// https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/storage.html
// https://stackoverflow.com/a/74688849
// https://www.baeldung.com/spring-security-authentication-with-a-database
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
            .csrf(csrf -> csrf.disable())
            .authorizeHttpRequests(req -> {
                req.requestMatchers(HttpMethod.POST, "/api/inscription").permitAll();
                req.anyRequest().authenticated();
            })
            .httpBasic(Customizer.withDefaults())
            .formLogin(form -> form.disable());
        return http.build();
    }

    @Bean
    UserDetailsService userDetailsService(AuthenticationInterface authenticationInterface) {
        return new ResellerDetailsService(authenticationInterface);
    }

    public record ResellerDetailsService(AuthenticationInterface authenticationInterface) implements UserDetailsService {

        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            var optHashedKey = authenticationInterface.getHashedAuthKeyForResellerName(username);
            return optHashedKey
                .map(hk -> new ResellerDetails(username, hk))
                .orElseThrow(() -> new UsernameNotFoundException("Nom ou clé d’authentification invalide"));
        }
    }

    public record ResellerDetails(String username, String password) implements UserDetails {

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return List.of();
        }

        @Override
        public String getUsername() {
            return username;
        }

        @Override
        public String getPassword() {
            return password;
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}
