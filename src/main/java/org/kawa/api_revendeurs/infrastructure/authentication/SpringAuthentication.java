package org.kawa.api_revendeurs.infrastructure.authentication;

import org.kawa.api_revendeurs.infrastructure.database.ResellerEntity;
import org.kawa.api_revendeurs.infrastructure.database.ResellerRepository;
import org.kawa.api_revendeurs.services.AuthenticationInterface;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class SpringAuthentication implements AuthenticationInterface {
    private final ResellerRepository resellerRepository;
    private final PasswordEncoder passwordEncoder;

    public SpringAuthentication(ResellerRepository resellerRepository, PasswordEncoder passwordEncoder) {
        this.resellerRepository = resellerRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void storeAuthKeyForResellerName(String username, UUID authKey) {
        var resellerEntity = resellerRepository.findById(username).orElseThrow();
        resellerEntity.setAuthKey(passwordEncoder.encode(authKey.toString()));
        resellerRepository.save(resellerEntity);
    }

    @Override
    public Optional<String> getHashedAuthKeyForResellerName(String username) {
        return resellerRepository.findById(username).map(ResellerEntity::getAuthKey);
    }
}
