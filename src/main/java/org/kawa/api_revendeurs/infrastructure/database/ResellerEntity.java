package org.kawa.api_revendeurs.infrastructure.database;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.UUID;

@Entity
@Table(name = "reseller") //User is a reserved keyword
public class ResellerEntity {
    @Id
    private String username;

    private String email;

    private String authKey; // hashed

    public ResellerEntity setAuthKey(String authKey) {
        this.authKey = authKey;
        return this;
    }

    public ResellerEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public ResellerEntity setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getAuthKey() {
        return authKey;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }
}
