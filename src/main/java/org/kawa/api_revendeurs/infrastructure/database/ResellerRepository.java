package org.kawa.api_revendeurs.infrastructure.database;

import org.springframework.data.repository.CrudRepository;

public interface ResellerRepository extends CrudRepository<ResellerEntity, String> {
}
