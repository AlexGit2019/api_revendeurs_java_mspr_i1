package org.kawa.api_revendeurs.infrastructure.notifications;

import jakarta.mail.MessagingException;

public class MailSendingException extends RuntimeException {
    public MailSendingException(Exception cause) {
        super("Erreur lors de l'envoi de la clé d'authentification", cause);
    }
}
