package org.kawa.api_revendeurs.infrastructure.notifications;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("register.mail")
public record MailProperties(String senderName, String senderEmail, String subject) {
}
