package org.kawa.api_revendeurs.infrastructure.notifications;

import jakarta.mail.MessagingException;
import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;
import org.kawa.api_revendeurs.domain.Reseller;
import org.kawa.api_revendeurs.services.NotificationInterface;
import org.kawa.api_revendeurs.services.AuthenticationInterface;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Component
public class MailQrCodeNotification implements NotificationInterface {
    private final AuthenticationInterface authenticationInterface;
    private final JavaMailSender mailSender;
    private final MailProperties mailProperties;

    public MailQrCodeNotification(AuthenticationInterface authenticationInterface, JavaMailSender mailSender, MailProperties mailProperties) {
        this.authenticationInterface = authenticationInterface;
        this.mailSender = mailSender;
        this.mailProperties = mailProperties;
    }

    @Override
    public void notifyCreation(Reseller reseller) {
        var authKey = generateAndStoreUserAuthKey(reseller);
        var qrCode = generateQrCodeOf(authKey);
        sendEmailWithQrCode(reseller, qrCode);
    }

    private UUID generateAndStoreUserAuthKey(Reseller reseller) {
        var authKey = UUID.randomUUID();
        authenticationInterface.storeAuthKeyForResellerName(reseller.getUsername(), authKey);
        return authKey;
    }

    private ByteArrayOutputStream generateQrCodeOf(UUID authKey) {
        return QRCode
            .from(authKey.toString())
            .to(ImageType.PNG)
            .withSize(420, 420)
            .stream();
    }

    // https://www.baeldung.com/spring-email
    // https://habr.com/en/articles/439176/
    private void sendEmailWithQrCode(Reseller reseller, ByteArrayOutputStream qrCode) {
        try {
            var mimeMessage = mailSender.createMimeMessage();
            var helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
            helper.addAttachment("qrcode.png", new ByteArrayResource(qrCode.toByteArray()));
            var htmlMsg = """
                <p>Bonjour, Voici votre clé d'authentification en format QR code&nbsp;:<br/>
                  <img src="cid:qrcode.png"></img></p>
                """;
            helper.setText(htmlMsg, true);
            helper.setTo(reseller.getEmail());
            helper.setSubject(mailProperties.subject());
            helper.setFrom(mailProperties.senderEmail());
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            throw new MailSendingException(e);
        }
    }
}
