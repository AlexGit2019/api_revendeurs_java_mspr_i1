package org.kawa.api_revendeurs.services;

import org.kawa.api_revendeurs.domain.Product;

import java.util.List;

public class ProductService {
    private final ProductStorageInterface productStorageInterface;

    public ProductService(ProductStorageInterface productStorageInterface) {
        this.productStorageInterface = productStorageInterface;
    }
    public List<Product> getProducts() {
        return productStorageInterface.getProducts();
    }

    public Product getProduct(String id) {
        return productStorageInterface.getProduct(id);
    }
}
