package org.kawa.api_revendeurs.services;

import org.kawa.api_revendeurs.domain.Reseller;

public class ResellerService {
    private ResellerStorageInterface resellerStorageInterface;

    public ResellerService(ResellerStorageInterface resellerStorageInterface) {
        this.resellerStorageInterface = resellerStorageInterface;
    }

    public void registerReseller(Reseller reseller, NotificationInterface notificationInterface) {
        this.resellerStorageInterface.runAtomically(() -> {
            resellerStorageInterface.saveReseller(reseller);
            notificationInterface.notifyCreation(reseller);
        });
    }
}
