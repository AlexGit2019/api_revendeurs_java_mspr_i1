package org.kawa.api_revendeurs.services;

import org.kawa.api_revendeurs.domain.Reseller;

public interface ResellerStorageInterface {

    void runAtomically(Runnable atomicAction);

    void saveReseller(Reseller reseller);
}
