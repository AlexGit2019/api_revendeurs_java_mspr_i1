package org.kawa.api_revendeurs.infrastructure.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.example.infrastructure.ErpProductDTO;
import org.example.infrastructure.ErpProductDetailsDTO;
import org.junit.jupiter.api.Test;
import org.kawa.api_revendeurs.domain.Reseller;
import org.kawa.api_revendeurs.infrastructure.api.serialization.ProductApiDto;
import org.kawa.api_revendeurs.infrastructure.erp.ApiProperties;
import org.kawa.api_revendeurs.services.AuthenticationInterface;
import org.kawa.api_revendeurs.services.ResellerStorageInterface;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ProductControllerIT {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ResellerStorageInterface storage;

    @Autowired
    private AuthenticationInterface auth;

    @Autowired
    private ObjectMapper jackson;

    @Autowired
    private ApiProperties erpConf;

    @MockBean
    private RestTemplate erpApi;

    @Test
    void nonAuthenticatedCallToProductsEndpointFailsWithHTTP401() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/products"))
            .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    void authenticatedCallWithUnreachableErpFailsWithHTTP502() throws Exception {
        // given
        var testUser = "a-user";
        var testKey = UUID.randomUUID();
        storage.saveReseller(new Reseller(testUser, "test-user@example.org"));
        auth.storeAuthKeyForResellerName(testUser, testKey);
        Mockito
            .when(erpApi.exchange(
                Mockito.eq(erpConf.productsUrl()), Mockito.eq(HttpMethod.GET),
                Mockito.isNull(), Mockito.any(ParameterizedTypeReference.class)))
            .thenThrow(new RestClientException("fake failure"));

        // when
        var result = mvc.perform(MockMvcRequestBuilders.get("/api/products")
                .with(SecurityMockMvcRequestPostProcessors.httpBasic(testUser, testKey.toString())))
            .andExpect(MockMvcResultMatchers.status().isBadGateway()).andReturn();

        // then
        Assertions.assertThat(result.getResponse().getContentAsString(StandardCharsets.UTF_8))
            .contains("Erreur d'accès aux données");
    }

    @Test
    void authenticatedCallWithInvalidErpResponseFailsWithHTTP502() throws Exception {
        // given
        var testUser = "a-user";
        var testKey = UUID.randomUUID();
        storage.saveReseller(new Reseller(testUser, "test-user@example.org"));
        auth.storeAuthKeyForResellerName(testUser, testKey);
        Mockito
            .when(erpApi.exchange(
                Mockito.eq(erpConf.productsUrl()), Mockito.eq(HttpMethod.GET),
                Mockito.isNull(), Mockito.any(ParameterizedTypeReference.class)))
            .thenReturn(null);

        // when
        var result = mvc.perform(MockMvcRequestBuilders.get("/api/products")
                .with(SecurityMockMvcRequestPostProcessors.httpBasic(testUser, testKey.toString())))
            .andExpect(MockMvcResultMatchers.status().isBadGateway()).andReturn();

        // then
        Assertions.assertThat(result.getResponse().getContentAsString(StandardCharsets.UTF_8))
            .contains("Erreur de récupération des produits");
    }

    @Test
    void authenticatedGetProductsSucceedsWithHTTP200() throws Exception {
        // given
        var dt1 = OffsetDateTime.parse("2021-01-01T00:00:00Z");
        var dt2 = OffsetDateTime.parse("2022-02-02T00:00:00Z");
        var erpDto1 = new ErpProductDTO(dt1, "item 1", new ErpProductDetailsDTO(BigDecimal.ONE.toString(), "about item 1", "yellow"), 50, "5");
        var erpDto2 = new ErpProductDTO(dt2, "item 2", new ErpProductDetailsDTO(BigDecimal.TEN.toString(), "about item 2", "blue"), 100, "10");
        var apiDto1 = new ProductApiDto("5", dt1, "item 1", BigDecimal.ONE, "about item 1", "yellow", 50);
        var apiDto2 = new ProductApiDto("10", dt2, "item 2", BigDecimal.TEN, "about item 2", "blue", 100);
        var testUser = "a-user";
        var testKey = UUID.randomUUID();
        storage.saveReseller(new Reseller(testUser, "test-user@example.org"));
        auth.storeAuthKeyForResellerName(testUser, testKey);
        Mockito
            .when(erpApi.exchange(
                Mockito.eq(erpConf.productsUrl()), Mockito.eq(HttpMethod.GET),
                Mockito.isNull(), Mockito.any(ParameterizedTypeReference.class)))
            .thenReturn(ResponseEntity.ok(List.of(erpDto1, erpDto2)));

        // when
        var result = mvc.perform(MockMvcRequestBuilders.get("/api/products")
                .with(SecurityMockMvcRequestPostProcessors.httpBasic(testUser, testKey.toString())))
            .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        var products = jackson.readValue(result.getResponse().getContentAsByteArray(), new TypeReference<List<ProductApiDto>>() {
        });

        // then
        Assertions.assertThat(products).containsExactlyInAnyOrder(apiDto1, apiDto2);
    }
}
