package org.kawa.api_revendeurs.infrastructure.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.mail.internet.MimeMessage;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.kawa.api_revendeurs.infrastructure.api.serialization.UserDto;
import org.kawa.api_revendeurs.infrastructure.database.ResellerEntity;
import org.kawa.api_revendeurs.infrastructure.database.ResellerRepository;
import org.kawa.api_revendeurs.infrastructure.notifications.MailProperties;
import org.kawa.api_revendeurs.spring.GreenMailJUnitExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@ContextConfiguration(initializers = GreenMailJUnitExtension.GreenMailPortInitializer.class)
class ResellerControllerIT {
    @Autowired
    @RegisterExtension
    public GreenMailJUnitExtension greenMail;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper jackson;

    @Autowired
    private ResellerRepository resellerRepository;

    @Autowired
    private MailProperties mailProperties;

    @Transactional
    @Test
    void registeringUserSavesItInDB() throws Exception {
        var body = jackson.writeValueAsBytes(new UserDto("toto", "mail@mail.com"));
        mvc.perform(MockMvcRequestBuilders
                .post("/api/inscription")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isCreated());
        Assertions
            .assertThat(resellerRepository.findById("toto").get())
            .extracting(ResellerEntity::getUsername, ResellerEntity::getEmail)
            .containsExactly("toto", "mail@mail.com");

        Awaitility.await().atMost(2, TimeUnit.SECONDS).untilAsserted(() -> {
            MimeMessage receivedMessage = greenMail.getReceivedMessages()[0];

            Assertions.assertThat(receivedMessage.getAllRecipients().length).isEqualTo(1);
            Assertions.assertThat(receivedMessage.getAllRecipients()[0].toString()).isEqualTo("mail@mail.com");
            Assertions.assertThat(receivedMessage.getFrom()[0].toString()).isEqualTo(mailProperties.senderEmail());
            Assertions.assertThat(receivedMessage.getSubject()).isEqualTo(mailProperties.subject());
        });
    }
}
